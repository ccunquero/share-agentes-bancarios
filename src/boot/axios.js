import Vue from 'vue'
import axios from 'axios'
import VueClipboard from 'vue-clipboard2'
 
Vue.use(VueClipboard)
Vue.prototype.$http = axios
