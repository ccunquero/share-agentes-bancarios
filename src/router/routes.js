
const routes = [
  {
    path: '/',
    component: () => import('src/components/process/Home.vue'),
   
    name: "Init",
    meta : {
      requiresAuth : false,
    }
  },
  {
    path: '/Login',
    component: () => import('src/components/security/Login.vue'),
   
    name : "Login",
    meta : {
      requiresAuth : false,
    }
  },
  {
    path: '/Home',
    component: () => import('src/components/process/Home.vue'),
  
    name: "Home",
    meta : {
      requiresAuth : true,
    }
  },
  {
    path: '/Loan',
    component: () => import('src/components/process/LoanPayment.vue'),
  
    name :"LoanPayment",
    meta :{
      requiresAuth : true,
    }
  },
  {
    path: '/Documents',
    component: () => import('src/components/process/Documents.vue'),
  
    name : "Documents",
    meta : {
      requiresAuth : true,
    },
  }
  
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes

