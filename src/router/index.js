import Vue from "vue";
import VueRouter from "vue-router";
import VueCurrencyInput from 'vue-currency-input'
import VueHtml2pdf from 'vue-html2pdf'
import VueExpandableImage from 'vue-expandable-image'
Vue.use(VueExpandableImage)
Vue.use(VueHtml2pdf)


const pluginOptions = {
  /* see config reference */
  globalOptions: { currency: 'USD',}
}

import { getItemStorage } from "../js/Util";

import routes from "./routes";
Vue.use(VueCurrencyInput, pluginOptions)
Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function(/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  Router.beforeEach((to, from, next) => {
  
    let isAuthenticated = getItemStorage("idUser") != null  ? true : false
 
    if (to.name !== 'Login' && !isAuthenticated) next({ name: 'Login' })
    else if(to.name == "Login" && isAuthenticated) next({name :  "Home"}) 
    else  next()

  })

 
  return Router;
}
