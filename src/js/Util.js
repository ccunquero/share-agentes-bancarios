import SecureLS from 'secure-ls';
//export const URL_BASE = "http://localhost:8787/bancamovilws/api/agentBank/"; //Local
//export const URL_BASE ="https://agenteshare.com/bancamovilws/api/agentBank/"; // Pruduccion
export const URL_BASE = "https://agenteprueba.net/bancamovilws/api/agentBank/" // Ambiente de pruebas
//export const URL_BASE = "http://181.114.12.130:8787/bancamovilws/api/agentBank/" //ambiente de pruebas tec nueva


export const Result = {
  USER_LINKED: 6, //Usuario vinculado en otro dispositivo
  USER_ERROR: 3, //Usuario incorrecto (nombre o password)
  ERROR_SERVER: 500, //Erorr en el servidor
  SUCCESS: 200, //Peticion realizada con exito
  TOKEN_ERROR: -2, //EL token no es valido
  PASSWORD_INCORRECT: -1, // La contraseña no es la correcta (En cambio de contraseña)
  VALID: 1, //Todos los datos a validar son validos
  NOT_FOUND: 404, //NO se ha encontrado
  USER_NOT_FOUND: -1, //Usuario no exite en validar token
  USER_BLOCKED: 5, //El usuario esta bloqueado
  INVALID_TOKEN_ASSOCIATED: 4, //El token no esta asociado
  USER_KEY_INVALID: 401 //El userkey no es  valido
};

export const bindMoney = {
    decimal: '.',
    thousands: ',',
    prefix: '',
    suffix: '',
    precision: 2,
    masked: false /* doesn't work with directive */
}

export async function request(context, urlComplement, body, hideLoading = false) {
  console.log(URL_BASE + urlComplement);
  console.log(body);
 
  let promise = new Promise((resolve, reject) => {
    if(!hideLoading) context.$q.loading.show();
    context.$http
      .post(URL_BASE + urlComplement, body, {
        headers: {
          "content-type": "application/json"
        }
      })
      .then(response => {
        context.$q.loading.hide();
        console.log(response);
        if (response) {
          if (response.data.estado == 401) {
            notifyErrorAuthentication(context);
            resolve(null);
          } else if (response.data.estado == 500) {
            notifyErrorServer(context);
            resolve(null);
          } else {
            resolve(response);
          }
        } else {
          resolve(null);
          notifyError(context);
        }
      })
      .catch(error => {
        console.log(error);
        resolve(null);
        notifyError(context);
    //    removeItemStorage("idUser")
     //   context.$router.push("Login") 
        context.$q.loading.hide();
      });
  });

  return await promise;
}
export function notifySuccess(context, message) {
  context.$q.notify({
    type: "positive",
    message: message ? message : "Operación realizada correctamente"
  });
}

export function notifyError(context, message) {
  context.$q.notify({
    type: "negative",
    message: message ? message : "Se ha producido un error"
  });
}

export function notifyErrorServer(context) {
  context.$q.notify({
    type: "negative",
    message: "Se ha producido un error en el servidor"
  });
}

export function notifyErrorAuthentication(context) {
  context.$q.notify({
    type: "negative",
    message: "Error en la autenticación del usuario"
  });
}

export function notifyWarning(context, message) {
  context.$q.notify({
    type: "warning",
    message: message ? message : "Advertencia"
  });
}

export function notifyInfo(context, message) {
  context.$q.notify({
    type: "info",
    message: message ? message : "Informacion"
  });
}
export function setItemStorage(itemName, itemValue) {
  var secureStorage = new SecureLS({ encodingType: "rc4" });
  secureStorage.set(itemName, itemValue);
}
export function getItemStorage(itemName) {
  try{
    var secureStorage = new SecureLS({ encodingType: "rc4" });
    var result = secureStorage.get(itemName)
    return (result.length == 0) ?  null : result;
  }catch(error){
    return null
  }
   
}

export function removeItemStorage(itemName) {
  var secureStorage = new SecureLS({ encodingType: "rc4" });
  secureStorage.remove(itemName);
}

export function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
};
export function base64toBlob(dataURI) {

  var byteString = atob(dataURI.split(',')[1]);
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);

  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }
  return new Blob([ab], { type: 'image/jpeg' });
}

export function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

export function  formatToParse(value) {
  return (value + "").split(",").join("") || 0;
}

function writeFile(text){
  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {

    console.log('file system open: ' + fs.name);
    fs.root.getFile("newPersistentFile.txt", { create: true, exclusive: false }, function (fileEntry) {

        console.log("fileEntry is file?" + fileEntry.isFile.toString());
        // fileEntry.name == 'someFile.txt'
        // fileEntry.fullPath == '/someFile.txt'
         // Create a FileWriter object for our FileEntry (log.txt).
        fileEntry.createWriter(function (fileWriter) {

          fileWriter.onwriteend = function() {
              console.log("Successful file write...");
           
          };

          fileWriter.onerror = function (e) {
              console.log("Failed file write: " + e.toString());
          };

           let dataObj = new Blob([text + "\n"], { type: 'text/plain' });
          fileWriter.write(dataObj  );
      
      });

    }, function (){});

},  function (){});
}
