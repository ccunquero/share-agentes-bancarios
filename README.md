# SHARE (agentes)

SHARE Agentes Bancarios

### Versiones requeridas
```bash
vue cli 4.5.x
cordova 10.0.0
cordova-android 9.0.0 (Imporante esta version para camara)
quasar cli 1.15.x
node js 12.x

```
## Instalar las dependencias
```bash
npm install
```

### Iniciar en modo desarrollo (web)
```bash
quasar dev
```

### Compilar para movil 
```bash

cordova build -m android (compiar para android )
cordova build -m ios (compilar para iOS)
cd src-cordova (luego entramos en la ruta donde esta cordova)
cordova build android (compilanos nuevamente para andorid, de lo contario no se puede instalar el apk por falta de firma)
cordova build ios  (compilanos nuevamente para ios)

Ahora ya podemos instalar el apk en nuestro dispositivo movil android, siempre teniendo en cuenta que debe estar configurado para aceptar aplicaciones que no vienen de la Play Store

Para iOS ya se ha generado el ipa, ahora ya se puede subir en App Connect y hacer las pruebas con TestFlight
```


### Compilar para produccion (web)
```bash
quasar build
```

### 

### Para ver mas configuraciones disponibles
 [Configurar quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
